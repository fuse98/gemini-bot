import os
from gem_bot import GemBot, handel_start, handel_question
from services.gemini import GeminiChatbotService


def main():
    google_api_key = os.environ["GOOGLE_API_KEY"]
    telbot_api_key = os.environ["TelBotSecretKey"]
    gemini_service = GeminiChatbotService(google_api_key)
    bot = GemBot(telbot_api_key, gemini_service)

    bot.register_message_handler(handel_start, commands=['start'], pass_bot=True)
    bot.register_message_handler(handel_question, func= lambda msg: True, pass_bot=True)

    bot.run()


if __name__ == '__main__':
    main()
