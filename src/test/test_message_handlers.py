import unittest
from unittest.mock import MagicMock
from gem_bot import handel_question


class TestHandelQuestion(unittest.TestCase):

    def setUp(self) -> None:
        self.test_question = 'What is gemini?'
        self.mock_message = MagicMock()
        self.mock_message.text = self.test_question
        return super().setUp()

    def test_message_with_text_should_call_gemini_service(self):
        mock_gemini = MagicMock()
        mock_bot = MagicMock()
        mock_bot.gemini_service = mock_gemini

        handel_question(message=self.mock_message, bot=mock_bot)

        mock_gemini.generate_content.assert_called_once_with(self.test_question)
