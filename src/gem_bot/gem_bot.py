from telebot import TeleBot

from services.gemini import GeminiChatbotService


class GemBot(TeleBot):

    def __init__(
        self, token: str,
        gemini_service: GeminiChatbotService,
    ):
        super().__init__(token=token)
        self.gemini_service = gemini_service

    def run(self):
        self.infinity_polling()
