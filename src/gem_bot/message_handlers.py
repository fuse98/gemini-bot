from telebot.types import Message
from google.api_core.exceptions import PermissionDenied
from gem_bot.gem_bot import GemBot
import texts


def handel_start(message: Message, bot: GemBot):
    tel_id = message.from_user.id
    bot.send_message(tel_id, texts.WELCOM)


def handel_question(message: Message, bot: GemBot):
    tel_id = message.from_user.id
    prompt = message.text

    if prompt is None:
        bot.send_message(tel_id, "Sorry! We couldn't understand your question. Questions must be only text.")
        return
    try:
        answer = bot.gemini_service.generate_content(prompt)
        bot.send_message(tel_id, answer)
    except PermissionDenied:
        bot.send_message(tel_id, "Google hates us :(((")
