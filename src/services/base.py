

class BaseChatbotService:

    def generate_content(self, prompt: str) -> str:
        raise NotImplementedError
