import google.generativeai as genai
from services.base import BaseChatbotService


class GeminiChatbotService(BaseChatbotService):

    def __init__(self, api_key: str, model: str='gemini-1.0-pro-latest') -> None:
        genai.configure(api_key=api_key)
        self._model = genai.GenerativeModel(model)

    def generate_content(self, prompt: str) -> str:
        response = self._model.generate_content(prompt)
        return response.text
